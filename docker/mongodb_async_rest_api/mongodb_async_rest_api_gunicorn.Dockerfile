FROM python:3.7.4

WORKDIR /mongodb_async_rest_api

RUN mkdir mongodb_async_rest_api

ADD mongodb_async_rest_api /mongodb_async_rest_api/mongodb_async_rest_api

RUN apt-get update && apt-get upgrade -y && apt-get install -y  \
    --no-install-recommends apt-utils \
    build-essential libssl-dev libffi-dev

RUN mkdir /project_tmp

ADD req.txt /project_tmp/requirements.txt
ADD /conf/gunicorn/gunicorn_conf.py /project_tmp/gunicorn_conf.py
ADD /conf/project_settings/local_settings.py /mongodb_async_rest_api/mongodb_async_rest_api/local_settings.py
RUN pip install --upgrade pip
RUN pip install -r /project_tmp/requirements.txt
ENV NAME mongodb_async_rest_api





