from aiohttp import web
from bson.objectid import ObjectId
from mongodb_async_rest_api.settings import TOKEN


async def authorize_token(request):
    tokens = request.app["tokens"]
    id = request.query.get('token')
    is_token = await tokens.find_one({"_id": ObjectId(id)})
    if not is_token:
        raise web.HTTPUnauthorized()
