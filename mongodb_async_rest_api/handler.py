from bson.objectid import ObjectId


def objectId_to_str(obj: dict) -> dict:
    obj["_id"] = str(obj["_id"])
    return obj


def str_to_objectId(obj: dict) -> dict:
    id = obj.get("_id")
    if id:
        obj["_id"] = ObjectId(id)
    return obj


def correct_sort(sort: list) -> list:
    sort = [(obj.get("key", ""), obj.get("type", 1)) for obj in sort if obj]
    return sort


def check_instance(query, _type: type):
    if not isinstance(query, _type):
        bad_request = "Expected dict, but {} received".format(type(query))
        raise Exception(bad_request)
