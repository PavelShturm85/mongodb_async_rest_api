from bson.objectid import ObjectId
from mongodb_async_rest_api.handler import objectId_to_str, str_to_objectId, correct_sort, check_instance
from aiohttp import web


async def find_one(collection: object, query: dict, args: list) -> dict:
    check_instance(query, dict)
    id = query.get("_id")
    if not id:
        raise web.HTTPBadRequest("Should use only \'_id\' key with \'find_one\' method.")

    return objectId_to_str(await collection.find_one({"_id": ObjectId(id)}))


async def find(collection: object, query: dict, args: list) -> list:
    check_instance(query, dict)
    sort, limit = args
    if sort and sort[0]:
        data = collection.find(str_to_objectId(query)).sort(correct_sort(sort))
    else:
        data = collection.find(str_to_objectId(query))
    return [objectId_to_str(obj) for obj in await data.to_list(length=int(limit))]


async def insert_one(collection, query: dict):
    check_instance(query, dict)
    await collection.insert_one(query)


async def insert_many(collection, query: list):
    check_instance(query, list)
    await collection.insert_many(query)


async def delete_one(collection, query: dict):
    check_instance(query, dict)
    await collection.delete_one(query)


async def delete_many(collection, query: dict):
    check_instance(query, dict)
    await collection.delete_many(query)


async def update_one(collection, query: dict, new_values: dict):
    check_instance(query, dict)
    await collection.update_one(query, new_values)


async def update_many(collection, query: dict, new_values: dict):
    check_instance(query, dict)
    await collection.update_many(query, new_values)


mongodb_actions_dict = dict(
    GET=dict(
        find_one=find_one,
        find=find,
    ),

    POST=dict(
        insert_one=insert_one,
        insert_many=insert_many,
    ),

    DELETE=dict(
        delete_one=delete_one,
        delete_many=delete_many,
    ),

    PUT=dict(
        update_one=update_one,
        update_many=update_many,
    ),

)
