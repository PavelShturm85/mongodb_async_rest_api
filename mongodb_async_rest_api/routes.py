from mongodb_async_rest_api.settings import mongodb
from mongodb_async_rest_api.views import MongodbRestApi, Tokens


def setup_routes(app, web):
    routes = [web.view('/{}'.format(collection), MongodbRestApi) for collection in mongodb["collections"]]
    routes.append(web.view('/token', Tokens))
    app.router.add_routes(routes)
