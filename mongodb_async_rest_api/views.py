import json
from aiohttp import web
from bson.objectid import ObjectId
from mongodb_async_rest_api.db_actions import mongodb_actions_dict
from mongodb_async_rest_api.authorize import authorize_token


class MongodbRestApi(web.View):

    def __init_collection(self, collection_name):
        return self.request.app[collection_name.replace("/", "")]

    async def get(self):
        try:
            await authorize_token(self.request)
            METHOD = self.request.method
            req_action = self.request.query.get("action")
            if not req_action:
                raise Exception("Not action.")
            req_query = self.request.query.get('query')
            req_sort = self.request.query.get("sort") or "[]"
            req_limit = self.request.query.get("limit") or "100"

            actions_for_current_method = mongodb_actions_dict.get(METHOD)
            action = actions_for_current_method.get(req_action)
            if action:
                collection = self.__init_collection(self.request.path)
                response_obj = await action(collection, json.loads(req_query),
                                            [json.loads(req_sort), int(json.loads(req_limit))])
            else:
                raise Exception("Not action.")

            return web.Response(text=json.dumps(response_obj), status=200)
        except Exception as e:
            response_obj = {'status': 'failed', 'reason': str(e)}
            return web.Response(text=json.dumps(response_obj), status=500)

    async def post(self):
        try:
            await authorize_token(self.request)
            METHOD = self.request.method
            req_action = self.request.query.get("action")
            if not req_action:
                raise Exception("Not action.")
            req_query = self.request.query.get('query')
            if not req_query:
                raise Exception("Not query for insert.")

            actions_for_current_method = mongodb_actions_dict.get(METHOD)
            action = actions_for_current_method.get(req_action)
            if action:
                collection = self.__init_collection(self.request.path)
                await action(collection, json.loads(req_query))
            else:
                raise Exception("Not action.")

            response_obj = {'status': 'success'}
            return web.Response(text=json.dumps(response_obj), status=200)
        except Exception as e:
            response_obj = {'status': 'failed', 'reason': str(e)}
            return web.Response(text=json.dumps(response_obj), status=500)

    async def put(self):
        try:
            await authorize_token(self.request)
            METHOD = self.request.method
            req_action = self.request.query.get("action")
            if not req_action:
                raise Exception("Not action.")
            req_query = self.request.query.get('query')
            if not req_query:
                raise Exception("Not query for update.")
            req_newvalues = self.request.query.get("newvalues")
            if not req_newvalues:
                raise Exception("Not new values for update.")

            actions_for_current_method = mongodb_actions_dict.get(METHOD)
            action = actions_for_current_method.get(req_action)
            if action:
                collection = self.__init_collection(self.request.path)
                await action(collection, json.loads(req_query), json.loads(req_newvalues))
            else:
                raise Exception("Not action.")

            response_obj = {'status': 'success'}
            return web.Response(text=json.dumps(response_obj), status=200)
        except Exception as e:
            response_obj = {'status': 'failed', 'reason': str(e)}
            return web.Response(text=json.dumps(response_obj), status=500)

    async def delete(self):
        try:
            await authorize_token(self.request)
            METHOD = self.request.method
            req_action = self.request.query.get("action")
            if not req_action:
                raise Exception("Not action.")
            req_query = self.request.query.get('query')
            if not req_query:
                raise Exception("Not query for delete.")

            actions_for_current_method = mongodb_actions_dict.get(METHOD)
            action = actions_for_current_method.get(req_action)
            if action:
                collection = self.__init_collection(self.request.path)
                await action(collection, json.loads(req_query))
            else:
                raise Exception("Not support action.")

            response_obj = {'status': 'success'}
            return web.Response(text=json.dumps(response_obj), status=200)
        except Exception as e:
            response_obj = {'status': 'failed', 'reason': str(e)}
            return web.Response(text=json.dumps(response_obj), status=500)


class Tokens(web.View):
    async def delete(self):
        try:
            await authorize_token(self.request)
            req_action = self.request.query.get("action")
            req_query = self.request.query.get('id')

            if not req_action:
                raise Exception("Not action.")
            elif not req_action in "delete_token":
                raise Exception("Not support action.")
            else:
                token = await self.request.app["tokens"].find_one_and_delete({"_id": ObjectId(req_query)})
                if not token:
                    raise Exception("Not token in db")

            response_obj = {'status': 'success'}
            return web.Response(text=json.dumps(response_obj), status=200)
        except Exception as e:
            response_obj = {'status': 'failed', 'reason': str(e)}
            return web.Response(text=json.dumps(response_obj), status=500)

    async def post(self):
        try:
            await authorize_token(self.request)
            req_action = self.request.query.get("action")
            if not req_action:
                raise Exception("Not action.")
            elif not req_action in "get_token":
                raise Exception("Not support action.")
            else:
                new_token = await self.request.app["tokens"].insert_one({"is_active": True, "type": "client"})

            response_obj = {"id": str(new_token.inserted_id)}
            return web.Response(text=json.dumps(response_obj), status=200)
        except Exception as e:
            response_obj = {'status': 'failed', 'reason': str(e)}
            return web.Response(text=json.dumps(response_obj), status=500)
