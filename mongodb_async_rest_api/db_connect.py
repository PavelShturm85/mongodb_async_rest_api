from mongodb_async_rest_api.settings import mongodb
import asyncio
from motor.motor_asyncio import AsyncIOMotorClient


@asyncio.coroutine
def setup_db():
    # https://motor.readthedocs.io/en/stable/tutorial-asyncio.html
    client = AsyncIOMotorClient(mongodb['ip'], mongodb['port'], connect=False)
    db = client[mongodb['db_name']]
    return db


def init_mongo_collection(app, db):
    def create_collection(app, db, collection):
        app[collection] = db[collection]

    [create_collection(app, db, collection) for collection in mongodb["collections"]]
    app["tokens"] = db["tokens"]
