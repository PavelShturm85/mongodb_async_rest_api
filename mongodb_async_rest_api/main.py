from aiohttp import web
import asyncio
from mongodb_async_rest_api.routes import setup_routes
from mongodb_async_rest_api.db_connect import init_mongo_collection, setup_db

loop = asyncio.get_event_loop()
db = loop.run_until_complete(setup_db())


async def web_app():
    app = web.Application()
    init_mongo_collection(app, db)
    setup_routes(app, web)
    return app
    # web.run_app(app)


if __name__ == '__main__':
    pass
# gunicorn main:web_app --bind localhost:3030 --worker-class aiohttp.worker.GunicornWebWorker --workers 7
