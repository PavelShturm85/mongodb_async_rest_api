"""gunicorn WSGI server configuration."""
from multiprocessing import cpu_count

vacuum = True
bind = "unix:/project_tmp/mongodb_async_rest_api/gunicorn.sock"

max_requests = 6000
max_requests_jitter = 2000

timeout = 25
graceful_timeout = 25

workers = 8 * cpu_count() + 1
worker_class = "aiohttp.worker.GunicornWebWorker"

uid = "www-data"
gid = "www-data"
